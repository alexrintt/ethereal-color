import { Color } from './components/color'
import { Palette } from './components/palette'
import { Gradient } from './components/gradient'
import { Converter } from './components/converter'

const EtherealColor = Object.freeze({
  Color,
  Palette,
  Gradient,
  Converter,
})

export default EtherealColor

// @ts-ignore
module.exports = EtherealColor
