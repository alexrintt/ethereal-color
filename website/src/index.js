import Hero from './components/hero'
import HeroCanvas from './components/hero-canvas'
import Demo1 from './components/demo-1'
import Demo2 from './components/demo-2'
import Demo3 from './components/demo-3'
import Footer from './components/footer'

window.addEventListener('DOMContentLoaded', () => {
  Hero()
  Demo1()
  Demo2()
  Demo3()
  HeroCanvas()
  Footer()
})
